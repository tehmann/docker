ARG SOURCE_TAG
FROM gitlab-registry.cern.ch/sft/docker/alma9-core:${SOURCE_TAG}
LABEL maintainer="project-lcg-spi-internal@cern.ch"
ENV container docker

# Install base packages
COPY builder/alma9/packages.txt /tmp/packages

# need epel-release for xrootd-client
# need systemd-devel (libudev for sherpa-openmpi)
RUN dnf update -y \
    && dnf install -y epel-release \
    && dnf install -y which systemd-devel $(cat /tmp/packages) \
    && dnf clean all \
    && rm -fv /tmp/packages

# Install krb5.conf (before installing krb5-workstation)
COPY builder/krb5.conf/rhel /etc/krb5.conf

# Some generators need f77 defined
RUN ln -sf /usr/bin/gfortran /usr/bin/f77

# try to run ldconfig
RUN ldconfig

# Add user sftnight, group sf and some subfolders in the $HOME folder
RUN groupadd -g 2735 sf \
    && useradd -u 14806 -ms /bin/bash sftnight \
    && usermod -g sf sftnight \
    && mkdir /home/sftnight/.ssh \
    && mkdir /home/sftnight/.ccache

# Setup SSH configuration for Jenkins
COPY misc/config /home/sftnight/.ssh/config
RUN chmod 600 /home/sftnight/.ssh/config \
    && chown -R sftnight:sf /home/sftnight/.ssh

# Might be needed for Jenkins: https://github.com/maxfields2000/dockerjenkins_tutorial/issues/18
RUN rm -fv /run/nologin

# Create the build area
RUN mkdir -p -m 777 /workspace
WORKDIR /workspace

# Run bash as user sftnight as default command
USER sftnight
CMD ["/bin/bash"]
