#!/usr/bin/env bash

build="docker build"
helpmsg=$'Usage: ./dockerbuild ls|build|get_command|export

ls                                                      - list all images to build 
build [--local-user] [-t TAG] [docker build args] IMG   - build specified image 
get_command [--local-user] [-t TAG] IMG                 - print the command to build 
export --tar|--registry IMAGE_NAME DEST                 - export image to tarfile or push to registry 
'

POSITIONAL=()
case $1 in
    ls|list)
        find . -name Dockerfile -exec dirname {} \; | cut -c3- | sort
        exit 0
        ;;
    build|get_command)
        if [[ "$1" =~ "build" ]]; then
            cmd=eval
        else
            cmd=echo
        fi
        shift
        LAST=""
        while [[ $# -gt 0 ]]
        do
        case $1 in
            --local-user)
                localusr="--build-arg user_id=$(id -u) --build-arg username=$(users | cut -d' ' -f1) --build-arg groupname=$(groups | egrep -o "sf[a-zA-Z]*") --build-arg group_id=$(id -g)"
                shift
                ;;
            *)
                POSITIONAL+=("$LAST")
                LAST=$1
                shift
                ;;
        esac
        done
        set -- "${POSITIONAL[@]}"
        echo "$cmd""-->" "$build $localusr $@ -f $LAST/Dockerfile ."
        $cmd $build $localusr $@ -f $LAST/Dockerfile .
        shift
        ;;
    export)
        case $2 in
            --registry)
                docker push $4:$3
                ;;
            --tar)
                docker save $3 > $4 
                ;;
            *)
                echo "Specify --registry or --tar! By default do nothing."
                echo "$helpmsg"
                exit 1
                ;;
        esac
        ;;
    *)
        echo "$helpmsg"
        exit 0
        ;;
esac
