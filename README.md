# SFT Docker Repository

This repository stores Dockerfiles and samples to build Docker images for SFT Software Operations

## Run gitlabCI to build docker container

The docker containers can be build directly in the gitlab CI by going to the Pipeline and starting the desired platforms.  
Only platforms are available to run for which the `build/<Platform>/` files were modified.

## Backup old docker images in GitLab registry

This command can be run anywhere.  

**Example:** `cc7` image

```bash
platform=cc7
timstamp=$( date "+%Y-%m-%d" )
docker pull gitlab-registry.cern.ch/sft/docker:lcg-${platform}
docker tag gitlab-registry.cern.ch/sft/docker:lcg-${platform} gitlab-registry.cern.ch/sft/docker:lcg-${platform}-backup-${timstamp}
docker login gitlab-registry.cern.ch
docker push gitlab-registry.cern.ch/sft/docker:lcg-${platform}-backup-${timstamp}
```

## Deploy new image to GitLab registry

This command must be run in the main folder of this repo.  

**Example:** `cc7` image

```bash
platform=cc7
docker rmi -f gitlab-registry.cern.ch/sft/docker:lcg-${platform}
docker build -t gitlab-registry.cern.ch/sft/docker:lcg-${platform} -f builder/${platform}/Dockerfile .
docker login gitlab-registry.cern.ch
docker push gitlab-registry.cern.ch/sft/docker:lcg-${platform}
```

## Old documentation

### Centos7 with cvmfs

The docker image `cc7-lcg-cvmfs` contains a snapshot of the configuration employed in the LCG build nodes
during the configuration, build and installation of the LCG software stack. It includes a standalone cvmfs
installation so the whole list of LCG releases is accessible even if the host (where the docker container
is running on) does not have cvmfs installed itself.

#### Prerequisites

Make sure you have already installed Docker in your host. You don’t need to install CVMFS since it
is provided by the Docker image.

For further details please have a look at the [official Docker documentation](https://docs.docker.com/engine/installation/).

#### Running a container

Since CVMFS needs to be mounted some special flags are required when starting a container:

```
docker run --privileged -it gitlab-registry.cern.ch/sft/docker:cc7-lcg-cvmfs bash
```

The previous command does the following:

1. Download the docker image from our private gitlab-registry (not from Dockerhub), if the most recent version of the image is not locally found.
2. Start up a docker container:
  - Using a privileged mode (`--privileged`) in order to mount cvmfs as a volume.
  - Running interactively (`-it`), so the process's standard input, output, and standard error are attached to the console.
3. Execute the `bash` command inside the container as the default user (`sftnight`)

Note that if `--privileged` is not specified, the container will start but cvmfs won't be mounted.

#### Problems or suggestions

Feel free to contact us for any suggestion or needed modification in the docker image.
