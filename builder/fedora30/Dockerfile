FROM fedora:30
LABEL maintainer="project-lcg-spi-internal@cern.ch"

# Install krb5.conf (before installing krb5-workstation)
COPY builder/krb5.conf/rhel /etc/krb5.conf

# Install base packages
COPY builder/fedora30/packages.txt /tmp/packages
RUN dnf update -y \
    && dnf install -y $(cat /tmp/packages) \
    && rm -fv /tmp/packages \
    && dnf clean all

# Set the correct timezone
RUN ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime

# Add user sftnight, group sf and some subfolders in the $HOME folder
RUN groupadd -g 2735 sf \
    && useradd -u 14806 -ms /bin/bash sftnight \
    && usermod -g sf sftnight \
    && mkdir /home/sftnight/.ssh \
    && mkdir /home/sftnight/.ccache

# Setup SSH configuration for Jenkins
COPY misc/config /home/sftnight/.ssh/config
RUN chmod 600 /home/sftnight/.ssh/config \
    && chown -R sftnight:sf /home/sftnight/.ssh

# Setup ccache
RUN mkdir -p /ccache \
    && chown sftnight:sf /ccache
COPY misc/ccache.conf /home/sftnight/.ccache/ccache.conf
RUN cp $( which ccache ) /usr/local/bin \
    && ln -s /usr/local/bin/ccache /usr/local/bin/gcc \
    && ln -s /usr/local/bin/ccache /usr/local/bin/g++ \
    && ln -s /usr/local/bin/ccache /usr/local/bin/cc \
    && ln -s /usr/local/bin/ccache /usr/local/bin/c++

# Create the build area
RUN mkdir -m 777 /workspace
WORKDIR /workspace

# Run bash as user sftnight as default command
USER sftnight
CMD ["/bin/bash"]
