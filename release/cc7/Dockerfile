FROM cern/cc7-base

ENV container docker

# Prepare centos to use systemd
RUN yum -y update; yum clean all
RUN yum -y install systemd; yum clean all; (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == systemd-tmpfiles-setup.service ] || rm -f $i; done); \
    rm -f /lib/systemd/system/multi-user.target.wants/*; \
    rm -f /etc/systemd/system/*.wants/*; \
    rm -f /lib/systemd/system/local-fs.target.wants/*; \
    rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
    rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
    rm -f /lib/systemd/system/basic.target.wants/*; \
    rm -f /lib/systemd/system/anaconda.target.wants/*; \
    rm -f /usr/lib/tmpfiles.d/systemd-nologin.conf;

# Install base packages
RUN yum install -y yum-plugin-ovl
RUN yum update -y \
    && yum -y install HEP_OSlibs \
    && yum clean all

ARG user_id=1000
ARG group_id=1000
ARG username=lcg
ARG groupname=lcggrp

RUN groupadd -g $group_id $groupname \
      && useradd -u $user_id -ms /bin/bash $username  \
      && usermod -g $groupname $username 

RUN mkdir -p /cvmfs/sft.cern.ch/lcg/releases && mkdir -p /cvmfs/sft.cern.ch/lcg/views
RUN rm -f /run/nologin

# Copy unpacked to image, has to be *.tar, otherwise might copy the tar not what's inside it
ARG tarfile=LCG.tar
ADD ${tarfile} /cvmfs/sft.cern.ch/lcg/releases/

# Setup view
RUN mkdir -p lcg && cd lcg  && \
    wget https://gitlab.cern.ch/sft/lcgcmake/raw/master/cmake/scripts/create_lcg_view.py && \
    wget https://gitlab.cern.ch/sft/lcgcmake/raw/master/cmake/scripts/create_lcg_view_setup_csh.in && \
    wget https://gitlab.cern.ch/sft/lcgcmake/raw/master/cmake/scripts/create_lcg_view_setup_sh.in && \
    python create_lcg_view.py -l /cvmfs/sft.cern.ch/lcg/releases -p $(ls -1 /cvmfs/sft.cern.ch/lcg/releases/**/**/ | grep x86_64 | head -1) -r "" -d -B /cvmfs/sft.cern.ch/lcg/view

COPY release/scripts/* /lcg/
# Install contrib
RUN bash /lcg/install_contrib.sh

# Post install step
RUN cp /lcg/lcg.sh /etc/profile.d/

USER $username
CMD ["/bin/bash"]
