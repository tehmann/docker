# SPI builder Docker images

This folder contains the recipes for all of our build environments for the SPI nightly jobs that build and deploy the LCG software stack.

## Scripts

The scripts `build.sh` and `verify.sh` are mainly meant to be run on our Jenkins CI server or in gitlab CI.
But here are some commands to run them locally.
Always make sure that you are at least a **Developer** of the `sft/docker` project on GitLab.
That's a requirement for pushing container images to the GitLab registry.

```bash
cd ${WORKSPACE}/sft/docker/builder
# alias docker="sudo docker"
PLATFORM="ubuntu19" # same as the sub-folder names here: cc7, slc6, fedora30, utility, ...
BACKUP="false"      # true or false
docker login gitlab-registry.cern.ch
source build.sh
docker logout gitlab-registry.cern.ch
```
