Ubuntu 18.04 LTS
================

## Packages

The packages are taken from the virtual build machine `sft-ubuntu-1804-1`:
```bash
dpkg -l | grep ^ii | awk '{print $2}' > sft-ubuntu-1804-1.packages
```


## Needed actions post-build

After running `docker build` to create the new ubuntu image, `kerberos`
has to be manually installed:

```
apt-get -y -q install krb5-user libkrb5-dev
```

Also, include from any other existing node the configuration of kerberos: `/etc/krb5.conf`

TODO: Find out a way to do so in the `Dockerfile`


Note: This is ubuntu16.04 Dockerfile copy
