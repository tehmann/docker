#!/bin/bash

PLATFORM=$(ls -1 /cvmfs/sft.cern.ch/lcg/releases/**/**/ | grep x86_64 | head -1)

OS=$(echo $PLATFORM | cut -d'-' -f2)
COMP=$(echo $PLATFORM | cut -d'-' -f3)

BINUTILS=binutils228

EOSCONTRIB="https://lcgpackages.web.cern.ch/lcgpackages/docker/contrib"
DWNLD_FAIL="Could not dowload compiler '$COMP'"

if ! wget -q "$EOSCONTRIB/$BINUTILS-${OS}.tar"; then
    echo "Could not download binutils"
    exit 1
fi

if ! wget -q "$EOSCONTRIB/${COMP}binutils-${OS}.tar"; then

    if [[ $COMP =~ *"clang"* ]]; then
        #Try by adding 0 to the name
        if ! wget -q "$EOSCONTRIB/${COMP}0binutils-${OS}.tar"; then
            echo $DWNLD_FAIL
            exit 1
        fi
    else
        echo $DWNLD_FAIL
        exit 1
    fi
fi

#TODO: discover that version of gcc clang needs
if [[ $COMP =~ *"clang"* ]]; then
    GCC=62binutils
    if ! wget -q "$EOSCONTRIB/gcc${GCC}-${OS}.tar"; then
        echo Could not download compiler gcc${GCC} required by $COMP
        exit 1
    fi
fi

#Unpack
TARPATH=$PWD
mkdir -p /cvmfs/sft.cern.ch/lcg/contrib
cd /cvmfs/sft.cern.ch/lcg/contrib
for tarfile in $TARPATH/*.tar; do
    tar -xf $tarfile
done

rm $TARPATH/*.tar

