# utility

This is a CentOS 7 based Docker image that is **not** used for builds but instead for deployment and other jobs.
Thus it doesn't contain the same build dependencies as the `cc7` image but rather tools such as `cvmfs`, `xrootd-client` and Kerberos (`kinit`).
Still it uses the same user and group (`sftnight` and `sf`) with the same IDs.
